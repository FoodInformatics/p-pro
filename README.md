# P-PRO structure 

Figure 1 gives a graphical overview of the main concepts and relations.  

 
![Alt text](PPRO structure.jpg?raw=true "Title")
Figure 1: Structure of P-PRO, including links to other ontologies 

The main concept within P-PRO is *processed material*. A *processed material* corresponds to a food substance containing proteins that has undergone at least one processing step in any way. In practice sample and food substance are also used. The *processed material* can have a *brand*, a *sample id* and a *material type*. The *processed material* can only occur in a certain quantity and thus it is connected to the *OM:Quantity* concept. This concept expresses, for example, in which fraction a certain substance, including protein, is present in a *processed material*. It can also quantify protein functionality, e.g. gelling ability. 

In contrast, a *raw material*  is a potential food product that is not yet processed and is not composed of more than one organism. It can have a *market class* and a *brand*. This *raw material* comes from a NCBI Taxon species, which can be further specified by its *variety* or *cultivar*. It contains (some part of) a plant, fungus, or animal. They can be linked to the Plant Ontology (PO), fungal structure ontology (FAO) and the animal structure ontology (Uberon). The raw material can reference a whole organism, like a pig, but also a part of an fungus, animal or plant, such as a pea of a certain variety or cultivar. 

These concepts of raw and processed material can be used as an ingredient for a *processingStep*. Processed materials can be the output of a *processingStep*. A *processing step* is a step that is part of a process. *​Processing step*s include the method used, one or more ingredients (*quantity*) and additives (*quantity*), a *duration*, *concentration*, *equipment*, *pH*, *temperature*, *material used*, and an output (*quantity*). It also contains a *next step*, so an order of processing steps can be defined. ​  

Furthermore, the *raw* and *processed material* can have many observed quantities, which are defined as OM *quantities*, such as solubility, viscosity, etc. Via the observed *quantity*, the amount of protein in the raw and processed material can be specified. The names of these proteins can be taken from PRO or ChEBI. In the observed quantities there are also concepts included about digestibility, such as the Amino Acid Score (AAS). There are specific ways to determine AAS, such as Protein Digestibility Corrected Amino Acid Score (PDCAAS) and Digestible Indispensable Amino Acid Score (DIAAS). ​Moreover, P-PRO also includes sensory intensity, which combines a specific sensory category, such as ‘bland’, ‘burnt’, ‘cheesy’, with a score for each of these categories. ​ 

Since it is important to keep track of how these quantities are measured, the concept *measurement was added. These *measurement*s can (but do not have to be) be done by a *sosa:Sensor*, and a *measurement* can have multiple *processing step*s. For example, this can be used to describe digestibility measurements, such as PDCAAS and DIAAS, since there are many different methods by which to measure digestibility. 
